
<html>
    <head>
        <title>Inserting Form Data Into MySQL Using PHP and AJAX</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>

        <script src="https://use.fontawesome.com/493ac5f29c.js"></script>
   
    </head>
    <body>
        <div class="container">
            <div class="row">                
                <div class="col-md-6">
                    <form id="my-form" action="" method="post">
                        <br>
                        <input class="form-control" type="text" name="name" placeholder="Name" required>
                        <br>
                        <input class="form-control" type="short" name="short" placeholder="short" required>
                        <br>
                        <button class="btn btn-success" type="submit">Insert</button>
                    </form>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-3">                
                    <div id="alertMsj"></div>
                </div>
            </div>
        </div>
    </body>
       <script>
            $(document).ready(function() {
                $("#my-form").submit(function(e) {
                    e.preventDefault();
                    $.ajax({
                        url: "insert.php",
                        method: "post",
                        data: $("form").serialize(),
                        dataType: "text",
                        success: function(strMessage) {
                            function alert(message, type, icon) {
                                var alertPlaceholder = document.getElementById('alertMsj')
                                var wrapper = document.createElement('div')
                                wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert"> <i class="fa fa-'+ icon +'" aria-hidden="true"></i> ' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'
                                alertPlaceholder.append(wrapper)
                            }
                            switch(strMessage){
                                case 'E1':
                                    alert('Valor invalido, por favor, verifique', 'warning', 'exclamation')
                                    setTimeout(function() {
                                        $(".alert").fadeOut(1500);
                                    },3000);
                                break;
                                case 'E2':
                                    alert('El nombre o email ya es existente', 'warning', 'exclamation')
                                    setTimeout(function() {
                                        $(".alert").fadeOut(1500);
                                    },3000);
                                break;
                                case 'E3':
                                    alert('No se puedo ejecutar la consulta', 'danger', 'exclamation-triangle')
                                    setTimeout(function() {
                                        $(".alert").fadeOut(1500);
                                    },3000);
                                break;
                                case 'OK':
                                    alert('El registro se agrego correctamente', 'success', 'check')
                                    setTimeout(function() {
                                        $(".alert").fadeOut(1500);
                                    },3000);
                                break;
                            }
                        }
                    });
                });
            });
        </script>
</html>